from django.conf.urls import url
from noot import views


urlpatterns = [
	url(r'^new$', views.new_noot, name='new_noot'),
	url(r'^update/(?P<pk>\d+)/$', views.NootUpdate.as_view(), name='update_noot'),
	url(r'^update/text/(?P<pk>\d+)/$', views.TextContentUpdate.as_view(), name='update_text'),
	url(r'^update/link/(?P<pk>\d+)/$', views.LinkContentUpdate.as_view(), name='update_link'),
	url(r'^update/calendar/(?P<pk>\d+)/$', views.CalendarContentUpdate.as_view(), name='update_calendar'),
]