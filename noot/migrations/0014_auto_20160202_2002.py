# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('noot', '0013_auto_20160202_1649'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calendarcontent',
            name='the_date',
            field=models.CharField(max_length=20, blank=True),
        ),
        migrations.AlterField(
            model_name='calendarcontent',
            name='the_time',
            field=models.CharField(max_length=20, blank=True),
        ),
    ]
