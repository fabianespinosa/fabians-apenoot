# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('noot', '0002_auto_20160118_1514'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='calendarcontent',
            options={'verbose_name_plural': 'Calendars'},
        ),
        migrations.AlterModelOptions(
            name='category',
            options={'verbose_name_plural': 'Categories'},
        ),
        migrations.AlterModelOptions(
            name='linkcontent',
            options={'verbose_name_plural': 'Links'},
        ),
        migrations.AlterModelOptions(
            name='noot',
            options={'verbose_name_plural': 'Noten'},
        ),
        migrations.AlterModelOptions(
            name='textcontent',
            options={'verbose_name_plural': 'Texts'},
        ),
    ]
