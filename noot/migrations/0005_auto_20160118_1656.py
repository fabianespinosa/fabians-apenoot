# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('noot', '0004_auto_20160118_1622'),
    ]

    operations = [
        migrations.AlterField(
            model_name='noot',
            name='categories',
            field=models.ManyToManyField(to='noot.Category', blank=True),
        ),
        migrations.AlterField(
            model_name='noot',
            name='tags',
            field=models.ManyToManyField(to='noot.Tag', blank=True),
        ),
    ]
