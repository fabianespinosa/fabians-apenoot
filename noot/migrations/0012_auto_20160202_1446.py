# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('noot', '0011_auto_20160202_1423'),
    ]

    operations = [
        migrations.AddField(
            model_name='calendarcontent',
            name='the_date',
            field=models.DateField(default=datetime.datetime(2016, 2, 2, 13, 46, 40, 715850, tzinfo=utc), blank=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='calendarcontent',
            name='the_time',
            field=models.TimeField(default=datetime.datetime(2016, 2, 2, 13, 46, 54, 443875, tzinfo=utc), blank=True),
            preserve_default=False,
        ),
    ]
