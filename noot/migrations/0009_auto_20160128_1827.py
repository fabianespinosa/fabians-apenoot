# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('noot', '0008_auto_20160128_1727'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calendarcontent',
            name='noten',
            field=models.ManyToManyField(related_name='cal_noten', to='noot.Noot', blank=True),
        ),
        migrations.AlterField(
            model_name='linkcontent',
            name='noten',
            field=models.ManyToManyField(related_name='link_noten', to='noot.Noot', blank=True),
        ),
        migrations.AlterField(
            model_name='textcontent',
            name='noten',
            field=models.ManyToManyField(related_name='text_noten', to='noot.Noot', blank=True),
        ),
    ]
