# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('noot', '0010_textcontent_owner'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calendarcontent',
            name='content',
            field=models.DateTimeField(blank=True),
        ),
        migrations.AlterField(
            model_name='linkcontent',
            name='content',
            field=models.URLField(blank=True),
        ),
    ]
