# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('noot', '0014_auto_20160202_2002'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='noot',
            options={'verbose_name_plural': 'Noten', 'ordering': ('-created',)},
        ),
        migrations.AddField(
            model_name='noot',
            name='the_cats',
            field=models.CharField(max_length=250, blank=True),
        ),
        migrations.AddField(
            model_name='noot',
            name='the_tags',
            field=models.CharField(max_length=250, blank=True),
        ),
    ]
