# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('noot', '0007_auto_20160119_1940'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calendarcontent',
            name='noten',
            field=models.ManyToManyField(related_name='cal_noten', to='noot.Noot'),
        ),
        migrations.AlterField(
            model_name='linkcontent',
            name='noten',
            field=models.ManyToManyField(related_name='link_noten', to='noot.Noot'),
        ),
        migrations.AlterField(
            model_name='textcontent',
            name='noten',
            field=models.ManyToManyField(related_name='text_noten', to='noot.Noot'),
        ),
    ]
