# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('noot', '0012_auto_20160202_1446'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calendarcontent',
            name='content',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='calendarcontent',
            name='the_date',
            field=models.DateField(),
        ),
    ]
