# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('noot', '0006_auto_20160118_2139'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='noot',
            options={'ordering': ('-modified',), 'verbose_name_plural': 'Noten'},
        ),
        migrations.AlterField(
            model_name='calendarcontent',
            name='status',
            field=models.CharField(choices=[('Pr', 'Private'), ('Pu', 'Public')], max_length=2, default='Pu'),
        ),
        migrations.AlterField(
            model_name='linkcontent',
            name='status',
            field=models.CharField(choices=[('Pr', 'Private'), ('Pu', 'Public')], max_length=2, default='Pu'),
        ),
        migrations.AlterField(
            model_name='noot',
            name='status',
            field=models.CharField(choices=[('Pr', 'Private'), ('Pu', 'Public')], max_length=2, default='Pu'),
        ),
        migrations.AlterField(
            model_name='textcontent',
            name='status',
            field=models.CharField(choices=[('Pr', 'Private'), ('Pu', 'Public')], max_length=2, default='Pu'),
        ),
    ]
