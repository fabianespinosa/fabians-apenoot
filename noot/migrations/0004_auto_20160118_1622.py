# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('noot', '0003_auto_20160118_1558'),
    ]

    operations = [
        migrations.AddField(
            model_name='calendarcontent',
            name='status',
            field=models.CharField(max_length=2, choices=[('Pr', 'Private'), ('Pu', 'Public'), ('Dr', 'Draft')], default='Pr'),
        ),
        migrations.AddField(
            model_name='linkcontent',
            name='status',
            field=models.CharField(max_length=2, choices=[('Pr', 'Private'), ('Pu', 'Public'), ('Dr', 'Draft')], default='Pr'),
        ),
        migrations.AddField(
            model_name='noot',
            name='status',
            field=models.CharField(max_length=2, choices=[('Pr', 'Private'), ('Pu', 'Public'), ('Dr', 'Draft')], default='Pr'),
        ),
        migrations.AddField(
            model_name='textcontent',
            name='status',
            field=models.CharField(max_length=2, choices=[('Pr', 'Private'), ('Pu', 'Public'), ('Dr', 'Draft')], default='Pr'),
        ),
    ]
