# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('noot', '0009_auto_20160128_1827'),
    ]

    operations = [
        migrations.AddField(
            model_name='textcontent',
            name='owner',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, blank=True, on_delete=django.db.models.deletion.SET_NULL, null=True),
        ),
    ]
