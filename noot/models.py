import re, string
from datetime import datetime, date, time
from django import forms
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q
from django.utils import timezone
from apecore.models import TimeStampedModel


PUBLISHING_STATUS = (
	('Pr','Private'),
	('Pu','Public'),
)

class NotenManager(models.Manager):
	def noten_of_user(self, user):
		"""Return a queryset of noten that this user owns"""
		return super(NotenManager, self).get_queryset().filter(
			Q(owner_id=user.id))


class Category(TimeStampedModel):
	name = models.CharField(max_length=50, unique=True)

	def __str__(self):
		return self.name

	class Meta:
		verbose_name_plural = "Categories"


class Tag(TimeStampedModel):
	name = models.CharField(max_length=50, unique=True)

	def __str__(self):
		return self.name


class Noot(TimeStampedModel):
	owner = models.ForeignKey(User)
	title = models.CharField(max_length=140)
	categories = models.ManyToManyField(Category, blank=True)
	tags = models.ManyToManyField(Tag, blank=True)
	status = models.CharField(max_length=2, default='Pu',
		choices=PUBLISHING_STATUS)
	objects = NotenManager()

	the_tags = models.CharField(max_length=250, blank=True)
	the_cats = models.CharField(max_length=250, blank=True)

	def make_tags(self, the_tags):
		print(Tag.objects.all().count())
		the_tags_string = the_tags.lower()
		the_tags_list = re.sub('[,]', ' ', the_tags_string).split()
		the_tags_set = set(the_tags_list)

		print('tags set:', the_tags_set)
		all_tags_set = set()
		
		for t in Tag.objects.all():
			all_tags_set.add(t.name)
		
		for tag in the_tags_set:
			if tag in all_tags_set:
				Tag.objects.get(name=tag).noot_set.add(self)
			else:
				Tag.objects.create(name=tag)
				Tag.objects.get(name=tag).noot_set.add(self)

		print(Tag.objects.all().count())

	def make_cats(self, the_cats):
		print(Category.objects.all().count())
		the_cats_list = re.sub('[,]', ' ', string.capwords(the_cats)).split()
		the_cats_set = set(the_cats_list)

		all_cats_set = set()
		
		for c in Category.objects.all():
			all_cats_set.add(c.name)
		
		for cat in the_cats_set:
			if cat in all_cats_set:
				Category.objects.get(name=cat).noot_set.add(self)
			else:
				Category.objects.create(name=cat)
				Category.objects.get(name=cat).noot_set.add(self)


	def set_owner(self, request):
		if self.request.user.is_authenticated:
			owner = self.request.user
		return owner

	def has_text_content(self):
		return self.text_noten.all().count()

	def has_link_content(self):
		return self.link_noten.all().count()

	def has_cal_content(self):
		return self.cal_noten.all().count()

	def __str__(self):
		return self.title

	class Meta:
		ordering = ('-created', )
		verbose_name_plural = "Noten"


class TextContent(TimeStampedModel):
	content = models.TextField()
	noten = models.ManyToManyField(Noot, blank=True, related_name='text_noten')
	status = models.CharField(max_length=2, default='Pu',
		choices=PUBLISHING_STATUS)
	owner = models.ForeignKey(User, blank=True, null=True, 
		limit_choices_to={'is_superuser': False}, on_delete=models.SET_NULL)

	def set_owner(self, request):
		if not self.request.user.is_superuser:
			owner = self.request.user
		return owner

	def __str__(self):
		return self.content

	class Meta:
		verbose_name_plural = "Texts"


class LinkContent(TimeStampedModel):
	content = models.URLField(blank=True)
	noten = models.ManyToManyField(Noot, blank=True, related_name='link_noten')
	status = models.CharField(max_length=2, default='Pu',
		choices=PUBLISHING_STATUS)

	def __str__(self):
		return self.content

	class Meta:
		verbose_name_plural = "Links"


class CalendarContent(TimeStampedModel):
	content = models.DateTimeField()
	noten = models.ManyToManyField(Noot, blank=True, related_name='cal_noten')
	status = models.CharField(max_length=2, default='Pu',
		choices=PUBLISHING_STATUS)

	the_date = models.CharField(max_length=20, blank=True)
	the_time = models.CharField(max_length=20, blank=True)

	def set_the_content(self, formdate, formtime):
		if formtime == None:
			dt = datetime.strptime(formdate, '%Y-%m-%d')
			self.the_time = ''
			self.content = timezone.make_aware(dt, timezone.get_current_timezone())
		else:
			if re.match(r'^\d\d:\d\d$', formtime):
				formtime += ':00' 
			dt_str = str(formdate) + '-' + str(formtime)
			dt = datetime.strptime(dt_str, '%Y-%m-%d-%H:%M:%S')
			self.content = timezone.make_aware(dt, timezone.get_current_timezone())


	def get_the_date(self):
		return self.the_date


	def __str__(self):
		if self.the_time == '':
			return self.content.strftime('%b, %d %Y')
		return self.content.strftime('%b, %d %Y - %H:%M')

	class Meta:
		verbose_name_plural = "Calendars"