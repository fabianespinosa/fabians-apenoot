from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.utils import encoding
from django.utils.decorators import method_decorator
from django.views.generic.edit import UpdateView

from .models import Noot, TextContent, LinkContent, CalendarContent, Tag
from .forms import NootForm, TextForm, LinkForm, CalForm


def home_page(request):
    return render(request, "home.html")


def view_noten(request):
	all_noten = Noot.objects.all()
	public_noten = Noot.objects.filter(status='Pu')
	if not request.user.is_superuser:
		return render(request, 'noten.html', {'noten': public_noten})
	else:
		return render(request, 'noten.html', {'noten': all_noten})


@login_required
def new_noot(request):
	if request.method == 'POST':
		noot = Noot(owner=request.user)
		textcontent = TextContent()
		linkcontent = LinkContent()
		calcontent = CalendarContent()

		form = NootForm(data=request.POST, instance=noot, prefix = 'primary')
		textform = TextForm(request.POST, instance=textcontent, prefix = 'txt')
		linkform = LinkForm(request.POST, instance=linkcontent, prefix = 'lnk')
		calform = CalForm(request.POST, instance=calcontent, prefix = 'cal')
		
		if form.is_valid() and textform.is_valid() and linkform.is_valid() and calform.is_valid():
			form.save()
			textform.save()
			textcontent.noten.add(noot)
			if noot.the_tags != '':
				noot.make_tags(noot.the_tags)
			if noot.the_cats != '':
				noot.make_cats(noot.the_cats)

			if linkcontent.content != '':
				linkcontent.save()
				linkcontent.noten.add(noot)
			if calcontent.the_date != None:
				print(calcontent.the_date)
				calcontent.set_the_content(calcontent.the_date, calcontent.the_time)
				calcontent.save()
				calcontent.noten.add(noot)
			return redirect('user_dashboard')
	else:
		form = NootForm(prefix = 'primary')
		textform = TextForm(prefix = 'txt')
		linkform = LinkForm(prefix = 'lnk')
		calform = CalForm(prefix = 'cal')
	
	context = {
		'form': form,
		'textform': textform,
		'linkform': linkform,
		'calform': calform,
	}

	if request.user.is_superuser:
		return render(request, 'new_noot.html', context)
	else:
		return redirect('home')


class NootUpdate(UpdateView):
	model = Noot
	form_class = NootForm
	template_name = "update_views/noot.html"
	success_url = reverse_lazy('user_dashboard')

	def render_to_response(self, context, **response_kwargs):
		"""
		Returns a response, using the `response_class` for this
		view, with a template rendered with the given context.
		If any keyword arguments are provided, they will be
		passed to the constructor of the response class.
		"""
		if self.request.user.is_superuser:
			response_kwargs.setdefault('content_type', self.content_type)
			return self.response_class(
				request=self.request,
				template=self.get_template_names(),
				context=context,
				using=self.template_engine,
				**response_kwargs
			)
		else:
			return redirect('home')

	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(NootUpdate, self).dispatch(*args, **kwargs)


class TextContentUpdate(UpdateView):
	model = TextContent
	form_class = TextForm
	template_name = "update_views/textcontent.html"
	success_url = reverse_lazy('user_dashboard')

	def render_to_response(self, context, **response_kwargs):
		if self.request.user.is_superuser:
			response_kwargs.setdefault('content_type', self.content_type)
			return self.response_class(
				request=self.request,
				template=self.get_template_names(),
				context=context,
				using=self.template_engine,
				**response_kwargs
			)
		else:
			return redirect('home')

	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(TextContentUpdate, self).dispatch(*args, **kwargs)


class LinkContentUpdate(UpdateView):
	model = LinkContent
	form_class = LinkForm
	template_name = "update_views/linkcontent.html"
	success_url = reverse_lazy('user_dashboard')

	def form_valid(self, form):
		"""
		If the form is valid, save the associated model.
		"""
		if self.object.content == '':
			self.object.delete()
		else:
			self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def render_to_response(self, context, **response_kwargs):
		if self.request.user.is_superuser:
			response_kwargs.setdefault('content_type', self.content_type)
			return self.response_class(
				request=self.request,
				template=self.get_template_names(),
				context=context,
				using=self.template_engine,
				**response_kwargs
			)
		else:
			return redirect('home')

	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(LinkContentUpdate, self).dispatch(*args, **kwargs)


class CalendarContentUpdate(UpdateView):
	model = CalendarContent
	form_class = CalForm
	template_name = "update_views/calendarcontent.html"
	success_url = reverse_lazy('user_dashboard')

	def form_valid(self, form):
		self.object.set_the_content(self.object.the_date, self.object.the_time)
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def render_to_response(self, context, **response_kwargs):
		if self.request.user.is_superuser:
			response_kwargs.setdefault('content_type', self.content_type)
			return self.response_class(
				request=self.request,
				template=self.get_template_names(),
				context=context,
				using=self.template_engine,
				**response_kwargs
			)
		else:
			return redirect('home')

	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(CalendarContentUpdate, self).dispatch(*args, **kwargs)
