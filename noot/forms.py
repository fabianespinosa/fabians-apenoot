import re
from django import forms
from django.forms import inlineformset_factory
from .models import Noot, Category, Tag, TextContent, LinkContent, CalendarContent


class NootForm(forms.ModelForm):
	the_tags = forms.CharField(
		required=False,
		label='Tags',
		widget=forms.fields.TextInput(attrs={
				'placeholder': "Tags separated with comma's",
				'class': 'form-control',
			})
	)
	the_cats = forms.CharField(
		required=False,
		label='Categories',
		widget=forms.fields.TextInput(attrs={
				'placeholder': "Tags separated with comma's",
				'class': 'form-control',
			})
	)

	class Meta:
		model = Noot
		exclude = ['owner',]
		fields = [
			'owner',
			'title',
			'status',
			'the_tags',
			'the_cats'
		]
		widgets = {
			'title': forms.fields.TextInput(attrs={
					'placeholder': "How it will be called?",
					'class': 'form-control',
				}),
			'status': forms.RadioSelect(),
		}


class TextForm(forms.ModelForm):
	class Meta:
		model = TextContent
		exclude = [
			'noten',
			'status',
			'owner',
		]
		fields = ['content']
		widgets = {
			'content': forms.Textarea(attrs={
				'placeholder': 'Add a Text!',
				'class': 'form-control',
			}),
		}


class LinkForm(forms.ModelForm):
	content = forms.CharField(
		required=False,
		label='Links/URLs',
		widget=forms.fields.TextInput(attrs={
				'placeholder': 'Add a link/url',
				'class': 'form-control',
			})
		)

	class Meta:
		model = LinkContent
		exclude = [
			'noten',
			'status',
			'owner',
		]


class CalForm(forms.ModelForm):
	the_date = forms.DateField(
		required=False,
		label='Date',
		input_formats=['%Y-%m-%d',], 
		widget=forms.fields.DateInput(attrs={
				'placeholder': 'yyyy-mm-dd',
				'class': 'form-control',
			})
		)
	the_time = forms.TimeField(
		required=False,
		label='Time',
		input_formats=['%H:%M',],
		widget=forms.fields.TimeInput(attrs={
				'placeholder': 'hh:mm',
				'class': 'form-control',
			})
		)

	class Meta:
		model = CalendarContent
		exclude = [
			'noten',
			'status',
			'owner',
			'content',
		]