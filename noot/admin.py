from django.contrib import admin
from .forms import NootForm, TextForm
from .models import Category, Tag, Noot, TextContent, LinkContent, CalendarContent


class TxtNootInline(admin.TabularInline):
	model = TextContent.noten.through


class LnkNootInline(admin.TabularInline):
	model = LinkContent.noten.through


class CalNootInline(admin.TabularInline):
	model = CalendarContent.noten.through


class NootAdmin(admin.ModelAdmin):
	readonly_fields = ('owner', 'tags', 'categories',)
	list_display = (
		'__str__',
		'status',
		'created',
		'modified',
		'has_text_content',
		'has_link_content',
		'has_cal_content',
	)
	inlines = [
		TxtNootInline,
		LnkNootInline,
		CalNootInline,
	]
	form = NootForm


class TextContentAdmin(admin.ModelAdmin):
	def get_queryset(self, request):
		qs = super(NootAdmin, self).get_queryset(request)
		if request.user.is_superuser:
			return qs
		return qs.filter(owner=request.user)


admin.site.register(Category)
admin.site.register(Tag)
admin.site.register(Noot, NootAdmin)
admin.site.register(TextContent)
admin.site.register(LinkContent)
admin.site.register(CalendarContent)