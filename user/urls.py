from django.conf.urls import url
from user import views

urlpatterns = [
	url(r'^dashboard$', views.user_home, name='user_dashboard'),
	url(r'^signup$', views.SignUpView.as_view(), name='user_signup'),
]