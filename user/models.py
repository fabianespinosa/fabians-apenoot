from django.db import models
from django.contrib.auth.models import Permission, User


PERMISSIONS = (
	'add_textcontent',
	'change_textcontent',
)


class Owner(User):
	def change_an_user(self, user):
		if user.is_superuser:
			user.is_staff = False
			user.is_superuser = False
		else:
			user.is_staff = True
			user.is_superuser = True

	class Meta:
		proxy = True



class Guest(User):

	@classmethod
	def create(cls, username):
		user = cls(username=username)
		user.is_staff = True
		user.save()
		for perm in PERMISSIONS:
			perm = Permission.objects.get(codename=perm)
			user.user_permissions.add(perm)
		return user


	class Meta:
		proxy = True