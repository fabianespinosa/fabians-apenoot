from django import forms
from .models import Owner


class GuestToOwnerUpgradeForm(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control'}))

	class Meta:
		model = Owner
		fields = ['password']
