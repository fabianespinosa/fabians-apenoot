from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView
from django.contrib.auth.forms import UserCreationForm
from django.core.urlresolvers import reverse_lazy
from noot.models import Noot, TextContent, LinkContent, CalendarContent, Category, Tag
from .models import Guest, Owner
from .forms import GuestToOwnerUpgradeForm


@login_required
def user_home(request):
	the_owner = Owner.objects.get(pk=request.user.id)
	guests = Guest.objects.filter(is_superuser=False)
	owners = Owner.objects.filter(is_superuser=True).exclude(pk=request.user.id)
	my_noten = Noot.objects.noten_of_user(the_owner)
	form = GuestToOwnerUpgradeForm()
	error_message = ''

	if request.method == 'POST':
		form = GuestToOwnerUpgradeForm()
		form_pass = request.POST.get('password')

		if request.POST.get('user'):
			user_to_change = Guest.objects.get(username=request.POST.get('user'))
			if the_owner.check_password(form_pass):
				the_owner.change_an_user(user_to_change)
				user_to_change.save()
			else:
				error_message='Oeps!, wrong owner password'
			
	context = {
		'owner': the_owner,
		'all_the_noten': my_noten,
		'guests': guests,
		'owners': owners,
		'form': form,
		'error_message': error_message,
	}

	if not request.user.is_superuser:
		return redirect('home')
	else:
		return render(request, "dashboard.html", context)



class SignUpView(CreateView):
	form_class = UserCreationForm
	template_name = "signup.html"
	success_url = reverse_lazy('user_dashboard')